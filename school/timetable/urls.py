from django.urls import path

from . import views
from django.contrib.auth import views as auth_views

app_name = 'timetable'
urlpatterns = [
    # path('', views.my_login, name='index'),
    path('login', auth_views.login, {'template_name': 'timetable/login.html'}, name='login'),
    path('login', auth_views.logout, {'template_name': 'timetable/login.html'}, name='logout'),
    path('home', views.home_page, name='home'),
    path('teacher', views.teacher_index, name='t_page'),
    path('teacher/register', views.registration, name='regis'),
    path('teacher/register/student/', views.StudentRegisterView.as_view(), name='s_signup'),
    path('teacher/register/teacher/', views.TeacherRegisterView.as_view(), name='t_signup'),
    path('teacher/groups', views.new_group, name='ngr'),
    path('teacher/communicate', views.new_communicate, name='ncom'),
    path('teacher/my_profile', views.person_profile, name='t_profile'),
    path('<int:id>/', views.group_participants, name='detail'),
    path('teacher/my_groups', views.teacher_groups, name='tgr'),
    path('teacher/new_participant', views.new_participant, name='new_part'),
    path('teacher/remove_participant', views.remove_participant, name='rm_part'),
    path('student', views.student_index, name='s_page'),
    path('student/my_groups', views.student_groups, name='s_groups'),
    path('student/my_profile', views.person_profile, name='s_profile'),
    path('student/infos', views.communicates_list, name='s_infos'),
]
